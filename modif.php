<?php include 'header.php';
include 'connexion.php'?>
<div class="container">
    <h1 class="text-center">Adduser</h1>
    <div class="row">
        <form class="adduser-form" action="adduser.php" method="post"></form>
    </div>
</div>
<?php
    $data = ['nom'=>htmlspecialchars($_POST['nom']),
            'prenom'=>htmlspecialchars($_POST['prenom']),
            'email'=>htmlspecialchars($_POST['email']),
            'ville'=>htmlspecialchars($_POST['ville']),
            'description'=>htmlspecialchars($_POST['description']),
            'telephone'=>htmlspecialchars($_POST['telephone'])
            ];

    $sql = "INSERT INTO user_tp7 (nom, prenom, email, ville, description, telephone) VALUES (:nom, :prenom, :email, :ville, :description, :telephone)";

    if (isset($_POST['sub'])) {
        try {
            if ($update = $bdh->prepare($sql)->execute($data)) {
                echo '<div class="success alert alert-success"><h1><strong>SUCCESS</strong></h1></div>';
                header('location:adduser.php');
            }
        } catch (\Exception $e) {
            die('Erreur : '.$e->getMessage());
        }
    }
?>
<?php include 'footer.php'?>
