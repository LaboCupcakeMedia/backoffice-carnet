<?php
$user = 'root';
$pass = 'root';
$dbname = 'user_TP7';
try
{
	// On se connecte à MySQL
	$bdh = new PDO('mysql:host=localhost;dbname=user_TP7;charset=utf8', $user, $pass);
    $bdh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arrête tout
        die('Erreur : '.$e->getMessage());
}
?>
